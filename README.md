# **With this addon, you will no longer be disturbed during the battle by annoying loot windows!**

## Chat commands:
* /lcl, / lootclicker - opens the settings window.
* /lcl hard reset - removes all addon settings.
* /lcl exclear - fast clearing of the exclusion list (in case you have a very large one and it takes a long time to remove everything manually).

## Installation
1. Download **[Latest Version](https://gitlab.com/Artur91425/LootClicker/-/archive/master/LootClicker-master.zip)**
2. Unpack the Zip file
3. Rename the folder "LootClicker-master" to "LootClicker"
4. Copy "LootClicker" into Wow-Directory\Interface\AddOns
5. Restart Wow

## Screenshots
<img src="https://user-images.githubusercontent.com/24303693/141698180-75d1d55f-e581-4f0f-9c9d-b4f7ec681273.jpg" width="48%">
<img src="https://user-images.githubusercontent.com/24303693/141698178-b8e35358-f783-4e27-8af9-9c7fa046c875.jpg" width="48%">
<img src="https://user-images.githubusercontent.com/24303693/141698176-e52eb2a6-a20f-4849-b29b-c7f6c31cf31e.jpg" width="48%">
