## Interface: 30300
## Title: LootClicker
## Version: 9.7
## Notes: Automatically presses buttons when rolling items.
## Notes-ruRU: Автоматически нажимает кнопки при розыгрыше добычи.
## Author: Artur91425
## X-Email: Artur91425@gmail.com
## X-License: MIT License
## X-Author-Server: WoW Circle 3.3.5 x5
## X-Category: Miscellaneous
## X-Localizations: enUS, ruRU
## X-Credits: Сарыч
## X-Website: https://vk.com/id65515748
## SavedVariablesPerCharacter: LootClickerDB

global_patches.lua
util.lua
locale.lua

init.lua
core.lua
options.lua
