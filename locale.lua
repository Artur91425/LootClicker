local private = select(2, ...)
private.locale = {}

private.locale['enUS'] = {
  ["Add an item"] = nil,
  ["Ascending"] = nil,
  ["ATTENTION! If you use any addons that analyze the messages listed below, then when this option is enabled, these addons will not work!"] = nil,
  ["Button priority for %s quality"] = nil,
  ["Descending"] = nil,
  ["Disable button click messages"] = nil,
  ["Disable roll result messages"] = nil,
  ["Disable win messages"] = nil,
  ["Disables printing of messages who won the item."] = nil,
  ["Disables printing roll result messages."] = nil,
  ["Disables the printing of messages who clicked which button for each item."] = nil,
  ["Drag the item, insert the link to the item, or enter the item ID to add it to the list."] = nil,
  ['Enable only for BOE items.'] = nil,
  ["Enable this quality"] = nil,
  ["Enable addon for items of this quality."] = nil,
  ["Enable 'smart' disenchant (with enchant)"] = nil,
  ["Enable 'smart' disenchant (without enchant)"] = nil,
  ["Enable 'smart' disenchant (only BOE)"] = nil,
  ["Enable addon"] = nil,
  ["Enable loot message filter"] = nil,
  ["Exception list"] = nil,
  ["Are you sure you want to reset |cffff0000ALL|r the settings of the |cff00ff00current|r character and reload the user interface?"] = nil,
  ["From the list of exceptions"] = nil,
  ["General"] = nil,
  ["if available"] = nil,
  ["If available, click on '%s' for all items."] = nil,
  ["Ignore"] = nil,
  ["Item settings for quality"] = nil,
  ["Level"] = nil,
  ["Mode '%s'"] = nil,
  ["Name"] = nil,
  ["NO ITEM DATA"] = nil,
  ["Only BOE"] = nil,
  ["Only BOP"] = nil,
  ["Modules"] = nil,
  ["Price"] = nil,
  ["Rarity"] = nil,
  ["SMART_DISENCHANT_WITHOUT_DESC"] = "Disenchant ONLY Bind on Pickup (BOP) items!\n\nThis mode is suitable for those who do not have the profession \"Enchanting\", but you want to disenchant items as efficiently as possible.\nFor all BOP items will be pressed (if available) 'Disenchant', for other 'Need', 'Greed' or 'Pass' (depending on the addon settings).\nThe idea is that if the item is not BOP, then you can disenchant it later, by doing it on another character or by asking a friend.",
  ["SMART_DISENCHANT_ONLY_BOE"] = "Disenchant ONLY Bind on Equipment (BOE) items!\n\nThis mode is suitable for those who want to disenchant BOE items as efficiently as possible.\nFor all BOE items will be pressed (if available) 'Need', 'Disenchant', 'Greed' or 'Pass' (depending on the addon settings).\nThe idea is that BOE item you can disenchant it later, by doing it on another character or by asking a friend.",
  ["SMART_DISENCHANT_WITH_DESC"] = "This mode is suitable for those who have the profession \"Enchanting\" and you want to disenchant items as efficiently as possible.\nFor all items will be pressed (if available) 'Need' overwise 'Disenchant'.",
  ["Sorting"] = nil,
  ["To try to request data from the server, click on this item."] = nil,
  ["Unlock anchor"] = nil,
  ["You receive"] = nil,
  ["You Won!"] = nil,

  ["About"] = nil,
  ["Author"] = nil,
  ["Category"] = nil,
  ["Click and press Ctrl-C to copy"] = nil,
  ["Credits"] = nil,
  ["Email"] = nil,
  ["License"] = nil,
  ["Localizations"] = nil,
  ["%s on the %s realm"] = nil,
  ["Version"] = nil,
  ["Website"] = nil,
  ["Miscellaneous"] = nil,
}

private.locale['ruRU'] = {
  ["Add an item"] = "Добавить предмет",
  ["Ascending"] = "По возрастанию",
  ["ATTENTION! If you use any addons that analyze the messages listed below, then when this option is enabled, these addons will not work!"] = "ВНИМАНИЕ! Если Вы используете какие-либо аддоны, которые анализируют сообщения перечисленные ниже, то при включении данной опции эти аддоны работать не будут!",
  ["Button priority for %s quality"] = "Приоритет нажатия кнопок для |3-1(%s) качества",
  ["Descending"] = "По убыванию",
  ["Disable button click messages"] = "Отключить сообщения нажатия кнопок",
  ["Disable roll result messages"] = "Отключить сообщения результатов бросков",
  ["Disable win messages"] = "Отключить сообщения выигрыша",
  ["Disables printing of messages who won the item."] = "Отключает печать сообщений, кто выиграл предмет.",
  ["Disables printing roll result messages."] = "Отключает печать сообщений о результате бросков.",
  ["Disables the printing of messages who clicked which button for each item."] = "Отключает печать сообщений, кто какую кнопку нажал для каждого предмета.",
  ["Drag the item, insert the link to the item, or enter the item ID to add it to the list."] = "Перетащите предмет, вставьте ссылку на предмет или введите ID предмета, чтобы добавить его в список.",
  ['Enable only for BOE items.'] = 'Включить только для Персональных при Надевании (ПпН) предметов.',
  ["Enable this quality"] = "Включить это качество",
  ["Enable addon for items of this quality."] = "Включить аддон для предметов этого качества",
  ["Enable 'smart' disenchant (with enchant)"] = "Включить 'умное' распыление (с Наложением чар)",
  ["Enable 'smart' disenchant (without enchant)"] = "Включить 'умное' распыление (без Наложения чар)",
  ["Enable 'smart' disenchant (only BOE)"] = "Включить 'умное' распыление (только ПпН)",
  ["Enable addon"] = "Включить аддон",
  ["Enable loot message filter"] = "Включить фильтр сообщений о добыче",
  ["Exception list"] = "Список исключений",
  ["Are you sure you want to reset |cffff0000ALL|r the settings of the |cff00ff00current|r character and reload the user interface?"] = "Вы уверены, что хотите сбросить |cffFF0000ВСЕ|r настройки |cff00ff00текущего|r персонажа и перезапустить пользовательский интерфейс?",
  ["From the list of exceptions"] = "Из списка исключений",
  ["General"] = "Общее",
  ["if available"] = "если доступно",
  ["If available, click on '%s' for all items."] = "Если доступно, на все предметы нажимаем '%s'.",
  ["Ignore"] = "Игнорировать",
  ["Item settings for quality"] = "Настройки для предметов качества",
  ["Level"] = "Уровень предмета",
  ["Mode '%s'"] = "Режим '%s'",
  ["Name"] = "Имя",
  ["NO ITEM DATA"] = "НЕТ ДАННЫХ О ПРЕДМЕТЕ",
  ["Only BOE"] = "Только ПпН",
  ["Only BOP"] = "Только ПпП",
  ["Modules"] = "Модули",
  ["Price"] = "Стоимость",
  ["Rarity"] = "Качество",
  ["SMART_DISENCHANT_WITHOUT_DESC"] = "Распыление ТОЛЬКО Персональных при Получении (ПпП) предметов!\n\nЭтот режим подходит для тех, у кого нет профессии \"Наложение чар\", но Вы хотите максимально эффективно распылять предметы.\nДля всех ПпП предметов будет нажато (если доступно) 'Распылить', на другие 'Мне это нужно', 'Не откажусь' или 'Отказаться' (в зависимости от настроек аддона).\nИдея заключается в том, что если предмет не ПпП, то его распылить можно позднее, сделав это на другом персонаже или попросив товарища.",
  ["SMART_DISENCHANT_ONLY_BOE"] = "Распыление ТОЛЬКО Персональных при Надевании (ПпН) предметов!\n\nЭтот режим подходит для тех, кто хочет эффективно распылять только ПпН предметы.\nДля всех ПпН предметов будет нажато (если доступно) 'Мне это нужно', 'Распылить', 'Не откажусь' или 'Отказаться' (в зависимости от настроек аддона).\nИдея заключается в том, что ПпН предмет распылить можно позднее, сделав это на другом персонаже или попросив товарища.",
  ["SMART_DISENCHANT_WITH_DESC"] = "Этот режим подходит для тех, у кого есть профессия \"Наложение чар\" и Вы хотите максимально эффективно распылять предметы.\nДля всех предметов будет нажато (если доступно) 'Мне это нужно', иначе будет нажато 'Распылить'",
  ["Sorting"] = "Сортировка",
  ["To try to request data from the server, click on this item."] = "Чтобы попробовать запросить данные с сервера, нажмите на этот предмет.",
  ["Unlock anchor"] = "Разблокировать якорь",
  ["You receive"] = "Ваша добыча",
  ["You Won!"] = "Вы выиграли!",

  ["About"] = "Об аддоне",
  ["Author"] = "Автор",
  ["Category"] = "Категория",
  ["Click and press Ctrl-C to copy"] = "Щелкните и нажмите Ctrl-C для копирования",
  ["Credits"] = "Благодарности",
  ["Email"] = "Почта",
  ["License"] = "Лицензия",
  ["Localizations"] = "Языки",
  ["%s on the %s realm"] = "%s с реалма \"%s\"",
  ["Version"] = "Версия",
  ["Website"] = "Сайт",
  ["Miscellaneous"] = "Разное",
}

private.locale['deDE'] = {
  ["You receive"] = "Ihr habt erhalten:",
  ["You Won!"] = "Gewonnen!",

  ["About"] = "Über",
  ["Author"] = "Autor",
  ["Category"] = "Kategorie",
  ["Click and press Ctrl-C to copy"] = "Klicken und Strg-C drücken zum kopieren.",
  ["Credits"] = "Credits",
  ["Email"] = "E-Mail",
  ["License"] = "Lizenz",
  ["Localizations"] = "Lokalisierungen",
  ["%s on the %s realm"] = "%s on the %s realm",
  ["Version"] = "Version",
  ["Website"] = "Webseite",
}

private.locale['esES'] = {
  ["You receive"] = "Has recibido:",
  ["You Won!"] = "Has ganado",

  ["About"] = "Sobre",
  ["Author"] = "Autor",
  ["Category"] = "Categoría",
  ["Click and press Ctrl-C to copy"] = "Clic y pulse Ctrl-C para copiar.",
  ["Credits"] = "Créditos",
  ["Email"] = "Email",
  ["License"] = "Licencia",
  ["Localizations"] = "Idiomas",
  ["%s on the %s realm"] = "%s on the %s realm",
  ["Version"] = "Versión",
  ["Website"] = "Sitio web",
}

private.locale['esMX'] = {
  ["You receive"] = "Recibiste",
  ["You Won!"] = "¡Has ganado!",

  ["About"] = "Sobre",
  ["Author"] = "Autor",
  ["Category"] = "Categoría",
  ["Click and press Ctrl-C to copy"] = "Clic y pulse Ctrl-C para copiar.",
  ["Credits"] = "Créditos",
  ["Email"] = "Email",
  ["License"] = "Licencia",
  ["Localizations"] = "Idiomas",
  ["%s on the %s realm"] = "%s on the %s realm",
  ["Version"] = "Versión",
  ["Website"] = "Sitio web",
}

private.locale['frFR'] = {
  ["You receive"] = "Vous avez reçu",
  ["You Won!"] = "Gagné !",

  ["About"] = "à propos de",
  ["Author"] = "Auteur",
  ["Category"] = "Catégorie",
  ["Click and press Ctrl-C to copy"] = "Cliquez et appuyez sur Ctrl-C pour copier",
  ["Credits"] = "Credits",
  ["Email"] = "E-mail",
  ["License"] = "Licence",
  ["Localizations"] = "Localisations",
  ["%s on the %s realm"] = "%s on the %s realm",
  ["Version"] = "Version",
  ["Website"] = "Site web",
}

private.locale['itIT'] = {
  ["You receive"] = "Ottieni:",
  ["You Won!"] = "Hai vinto!",

  ["About"] = "Licenza",
  ["Author"] = "Autore",
  ["Category"] = "Category",
  ["Click and press Ctrl-C to copy"] = "Fare clic e premere Ctrl-C per copiare",
  ["Credits"] = "Credits",
  ["Email"] = "E-mail",
  ["License"] = "Licenza",
  ["Localizations"] = "Localizzazioni",
  ["%s on the %s realm"] = "%s on the %s realm",
  ["Version"] = "Versione",
  ["Website"] = "Sito Web",
}

private.locale['koKR'] = {
  ["You receive"] = "다음을 획득했습니다.",
  ["You Won!"] = "획득!",

  ["About"] = "대하여",
  ["Author"] = "저작자",
  ["Category"] = "분류",
  ["Click and press Ctrl-C to copy"] = "클릭하고 Ctrl-C를 눌러 복사",
  ["Credits"] = "Credits",
  ["Email"] = "전자 우편",
  ["License"] = "라이센스",
  ["Localizations"] = "현지화",
  ["%s on the %s realm"] = "%s on the %s realm",
  ["Version"] = "버전",
  ["Website"] = "웹 사이트",
}

private.locale['ptBR'] = {
  ["You receive"] = "Você recebeu",
  ["You Won!"] = "Você venceu!",

  ["About"] = "Sobre",
  ["Author"] = "Autor",
  ["Category"] = "Categoria",
  ["Click and press Ctrl-C to copy"] = "Clique e pressione Ctrl-C para copiar",
  ["Credits"] = "Credits",
  ["Email"] = "E-mail",
  ["License"] = "Licença",
  ["Localizations"] = "Localizações",
  ["%s on the %s realm"] = "%s on the %s realm",
  ["Version"] = "Versão",
  ["Website"] = "Site",
}

private.locale['zhCN'] = {
  ["You receive"] = "你获得了",
  ["You Won!"] = "你获得了：",

  ["About"] = "关于",
  ["Author"] = "作者",
  ["Category"] = "分类",
  ["Click and press Ctrl-C to copy"] = "点击并 Ctrl-C 复制",
  ["Credits"] = "鸣谢",
  ["Email"] = "电子邮件",
  ["License"] = "许可",
  ["Localizations"] = "本地化",
  ["%s on the %s realm"] = "%s on the %s realm",
  ["Version"] = "版本",
  ["Website"] = "网站",
}

private.locale['zhTW'] = {
  ["You receive"] = "你獲得",
  ["You Won!"] = "你贏得了：",

  ["About"] = "關於",
  ["Author"] = "作者",
  ["Category"] = "類別",
  ["Click and press Ctrl-C to copy"] = "左鍵點擊並按下 Ctrl-C 以複製字串",
  ["Credits"] = "貢獻者",
  ["Email"] = "電子郵件",
  ["License"] = "授權",
  ["Localizations"] = "本地化",
  ["%s on the %s realm"] = "%s 在「%s」伺服器",
  ["Version"] = "版本",
  ["Website"] = "網站",
}
